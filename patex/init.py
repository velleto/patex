# patex initialisation code.
# Copyright (C) 2018 Ryan Rueger and Contributors.

import re
import os
import sys
import argparse

import patex.utils

class PaTeX(object):
    """The main class that encapsulates the high level use of PaTeX.

    To run patex, call:

    >>> from patex import PaTeX

    >>> pt = PaTeX()
    >>> pt.run()
    """

    def run(self):
        """Invoke everything."""
        args = self.__parse_options()

        flex = self.__flatten(args.tex)
        rex = self.__replace(flex, args.replace)
        envex = self.__envonly(rex, args.envs, args.keep, args.lines)

        env = os.path.basename(args.tex) + ".env"
        if os.path.isfile(env):
            print("PaTeX WARNING: File already exists: {}".format(env))
            sys.exit(1)

        try:
            with open(env, "w") as env_file:
                for line in envex:
                    env_file.write(line)
        except:
            print("PaTeX ERROR: Error writing to file: {}".format(env))
            sys.exit(1)

        print("PaTeX SUCCESS: Finished.")

    def __parse_options(self):
        """Parse command line options."""
        parser = argparse.ArgumentParser()

        parser.add_argument('tex',
                            metavar="TEXFILE",
                            help="Specify path to the root TEX file.")

        parser.add_argument('envs',
                            metavar="ENV",
                            nargs="+",
                            help="Specify which environments should be kept. "
                                    "For example 'definition' or 'lemma' "
                                    "to keep content  enclosed in "
                                    "\\begin{definition} or \\begin{lemma} "
                                    "statements.")

        parser.add_argument('-l', '--lines',
                            metavar="<begin>:<end>",
                            help="Specify range in which environments "
                                    "should be removed. The default is "
                                    "the whole document range enclosed in "
                                    "\\begin{document} and \\end{document}.",
                            default=None)

        parser.add_argument('-k', '--keep',
                            metavar="KEEP",
                            nargs="*",
                            help="Specify which commands should be kept. "
                                    "For example, it may be favourable to "
                                    "keep chapter titles. For this pass "
                                    "'chapter' - to keep the \\chapter{} "
                                    "command.")

        parser.add_argument('-r', '--replace',
                            metavar="{<find>}:{<replace>}",
                            nargs="*",
                            help="Replace specific lines. Useful for "
                                    "redefining certain commands. "
                                    "A canonical example may be to "
                                    "remove numbering, as it no longer "
                                    "matches that of the original "
                                    "document because environments "
                                    "between have been omitted. "
                                    "(Note the braces).",
                            default=None)

        args = parser.parse_args()

        # Most shells will already expand this, however for good measure...
        args.tex = os.path.expanduser(args.tex)

        return args

    def __flatten(self, tex_path):
        """Flatten Tex source tree.

        Creates a single flattened Tex file from a project containing many
        inter-referencing Tex files.
        """
        basedir = os.path.dirname(tex_path)

        try:
            with open(tex_path, "r") as tex_file:
                tex = tex_file.readlines()
        except FileNotFoundError as error:
            print("PaTeX ERROR: ", error)
            sys.exit(1)

        # Regular Expression for 'input' or 'include statements.
        # Match any preceeding or trailing whitespace too.
        # Examples:
        # \input{file/name/path}
        # \include{file/name/path.tex}
        # Named filename capture group 'file'.
        input_re = r'\s*\\(input|include){(?P<file>[^}]*)}*\s*$'

        for n, line in enumerate(tex):

            input_match = re.match(input_re, line)

            if input_match:
                input_filename = input_match.group('file')
                input_filename = os.path.join(basedir, input_filename)

                # Some implementations of (La)Tex allow the user to omit the
                # .tex extension. Add this here if so.
                if not re.match(r'.*tex$', input_filename):
                    input_filename = input_filename + ".tex"

                try:
                    with open(input_filename, "r") as input_file:
                        tex[n:n+1] = input_file.readlines()
                except FileNotFoundError as error:
                    tex[n] = "% " + tex[n]
                    print(error)
                    print("PaTeX WARNING: (skipping):  " + input_filename)

        return tex

    def __envonly(self, rex, envs, keeps, lines=None):
        """Remove all environments not specified in envs."""
        # Do nothing if all environments are specified.
        # Useful for just creating a flat TeX file.

        if envs == ["all"]:
            return rex

        # Prepare environment-only TeX file.
        envex = []

        # Get line numbers of the start and end of the document.
        if lines:
            begin_n, end_n = patex.utils.parse_line_range(lines)

        # If a line range is not specified, or the range is specified
        # as being relative to begin or end, the explicit
        # line numbers still need to be found.
        if not lines or type(begin_n) is str:
            begin_re = r'\s*\\begin{document}\s*'
            begin_n = next(i for i, line in enumerate(rex)
                            if re.match(begin_re, line))

        if not lines or type(end_n) is str:
            end_re = r'\s*\\end{document}\s*'
            end_n = next(i for i, line in enumerate(rex)
                            if re.match(end_re, line))

        # Create the REs for each environment.
        env_res = [r'\s*\\(begin|end){{{}}}.*'.format(env)
                    for env in envs]

        # Create the REs for each statement to be kept.
        keep_res = [r'\s*\\{}{{.*}}.*'.format(keep)
                    for keep in keeps]

        include = False
        for n, line in enumerate(rex[begin_n:end_n+1]):

            env_matches = [re.match(env_re, line) for env_re in env_res]
            keep_matches = [re.match(keep_re, line) for keep_re in keep_res]
            env = any(env_matches)
            keepmatch = any(keep_matches)

            if keepmatch or env or include:
                envex.append(line)
                if env:
                    include = not include

        envex = rex[:begin_n+1] + envex + rex[end_n:]

        return envex

    def __replace(self, flex, replace):
        """Replace certain lines in the flattened file.

        This is especially useful for redefining certain commands.
        A canonical example may be to remove numbering, as it no longer
        matches that of the original document because environments between
        have been omitted."""
        if not replace:
            return flex

        matches = [re.match(r'^{(.*)}:{(.*)}$', rep) for rep in replace]
        try:
            find = [match.group(1) for match in matches]
            replace_with = [match.group(2) for match in matches]
        except AttributeError:
            print("PaTeX ERROR: Could not parse: " + " ".join(replace))
            sys.exit(1)

        replace_res = ['\s*{}\s*'.format(re.escape(fi)) for fi in find]

        for n, line in enumerate(flex):
            matches = [re.match(replace_re, line) for replace_re in replace_res]
            for i, replace_re in enumerate(matches):
                if replace_re:
                    flex[n] = replace_with[i]

        return flex
