import sys
import re


def parse_line_range(lines):
    """Parse line range to integer tuple."""
    match = re.match(r"(?P<begin>[0-9]*|begin):(?P<end>[0-9]*|end)$", lines)

    if not match:
        print("patex.utils.parse_line_range ERROR: Could not parse: " + lines)
        print("patex.utils.parse_line_range: Needs to be in form <int>:<int>.")
        sys.exit(1)

    m_begin = match.group("begin")
    m_end = match.group("end")

    begin = int(m_begin) if m_begin != "begin" else "begin"
    end = int(m_end) if m_end != "end" else "end"

    return (begin, end)
