# Include only specific environments in a `LaTex` document.

This project is **_work in progress_**.  
Currently there is no stable version available.

### Contributing

There is no `develop` branch in this repository. Only feature branches are opened directly from master, and then merged in.  
Release worthy versions will be cherry-picked onto the release branch, packaged with python and pushed to [PyPI](https://pypi.org/) so they can be installed with `pip`.
