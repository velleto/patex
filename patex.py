#!/usr/bin/env python3
# Startup from single-user installation
# Copyright (C) 2018 Ryan Rueger

from patex import PaTeX

pt = PaTeX()
pt.run()
