#!/usr/bin/env python3

import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="patex",
    version="0.0.1",
    author="Ryan Rueger",
    author_email="dev@velleto.com",
    description="Tex Parser. Include only specific environments in a Tex document.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.velleto.com/patex/",
    packages=setuptools.find_packages(),
    license="GPLv3",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPLv3 License",
        "Operating System :: OS Independent",
    ],
    scripts=["bin/patex"],
    include_package_data=True,
)
